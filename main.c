/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC12F1822
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

/*
 *******************************************************************************
 *       Project: Square_Wave_Generator
 *       Description: it generates a square wave with pulse-width modulation or 
 *                    frequency modulation in base of input selected
 *       Target: PIC12F1822
 *       Toolchain: 
 *                  MPlabX_v5.30; 
 *                  XC8_v2.05; 
 *                  Packs:12-16F1xxxx_DFP (1.0.42);
 *                  Mplab Code Configurator: v3.95.0
 *                                           Core v4.75
 *                                           PeripheralLibrary v1.77.0
 * 
 *       Started: 07/03/2020
 *       
 *       Note =  First release.
 *
 *******************************************************************************
 *******************************************************************************
 *       Vers = 0.0.1 - SW_square_wave_generator - 001
 *       Description = 
 *              - Implemented frequency_modulation_generator
 *  
 *       Date = 08/07/2020
 *       Author = --
 * 
 *******************************************************************************
 */

/******************************************************************************
* Includes
******************************************************************************/
#include "mcc_generated_files/mcc.h"
#include "sources/variables.h"
#include "sources/MCCinterface.h"
#include "sources/init.h"
#include "sources/analog.h"

/******************************************************************************
* Literals
******************************************************************************/
#define FREQUENCY_1000HZ   0
#define FREQUENCY_500HZ    1
#define FREQUENCY_200HZ    5
#define FREQUENCY_100HZ    10
#define FREQUENCY_10HZ     100
#define MAX_FREQUENCY      FREQUENCY_200HZ    

#define PERIOD_100MSEC     20
#define PERIOD_10MSEC      2
#define PERIOD_13MSEC      26   /* 130ms/5 */
#define MAX_PERIOD         PERIOD_13MSEC  

/******************************************************************************
* Local variables
******************************************************************************/

/******************************************************************************
* Local function prototypes
******************************************************************************/
void frequency_modulation_generator(void);
void pulse_width_modulation_generator(const uint32_t localPeriod, analogVar_t *analogLocalVar);

/*
                         Main application
 */
void main(void)
{
    // initialize the device
    SYSTEM_Initialize();

    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

    SystemInit();
    CLRWDT();
    while (1)
    {
        /***************/
        /* 500us START */
        if (timebase_flags.time_500us == true)
        {
            timebase_flags.time_500us = false;
            
            //OUT_SQUARE_RA4_Toggle(); /* toggle square pin to understand if firmware works */
            
            /* */            
            if (JUMPER_RA1_GetValue() == false)
            {
                pulse_width_modulation_generator(MAX_PERIOD, &analogVar);
            }
        }
        /* 500us END */
        /*************/

        
        /*************/        
        /* 1ms START */
        if (timebase_flags.time_1ms == true)
        {
            timebase_flags.time_1ms = false;
            
            /* */
            if(JUMPER_RA1_GetValue() == true)
            {
                frequency_modulation_generator();
            }
        }
        /* 1ms END */
        /***********/

        
        /**************/
        /* 10ms START */
        if (timebase_flags.time_10ms == true)
        {
            timebase_flags.time_10ms = false;
            
            /* */
            ReadAnalog(&analogVar);
        }
        /* 10ms END */
        /************/

        
        /***************/
        /* 100ms START */
        if (timebase_flags.time_100ms == true)
        {
            timebase_flags.time_100ms = false;
        }
        /* 100ms END */
        /*************/

        
        /***************/
        /* 500ms START */
        if (timebase_flags.time_500ms == true)
        {
            timebase_flags.time_500ms = false;
            
            //OUT_SQUARE_RA4_Toggle(); /* toggle square pin to understand if firmware works */
            CLRWDT();
        }
        /* 500ms END */
        /*************/

        
        /************/
        /* 1s START */
        if (timebase_flags.time_1s == true)
        {
            timebase_flags.time_1s = false;
        }
        /* 1s END */
        /**********/
    }
}

/**
  @Summary
    frequency_modulation_generator

  @Description
    Generate a frequency modulated square wave
    
  @Preconditions
     Must be called every 1ms   
  
  @Returns
    None

  @Param
    localFrequency: const to define the max frequency
    analogLocalVar: pointer to work variable

  @Example
    <code>
    </code>
*/
void frequency_modulation_generator()
{
    static uint16_t counter_frequency = 0;
    
	counter_frequency ++;       /* increase counter */
    
    OUT_SQUARE_RA4_Toggle();    /* toggle square pin to understand if firmware works */
    
    if (counter_frequency > analogVar.convertedValue)   /* reached threshold */
    {
        OUT_SING_RA5_Toggle();      /* Toggle pin */
        counter_frequency = 0;      /* Reset counter */
    }
}

/**
  @Summary
    pulse_width_modulation_generator

  @Description
    Generate pulse width modulation square wave
    
  @Preconditions
     Must be called every 100�s   
  
  @Returns
    None

  @Param
    localPeriod: const to define the max period
    analogLocalVar: pointer to work variable

  @Example
    <code>
    </code>
*/
void pulse_width_modulation_generator(const uint32_t localPeriod, analogVar_t *analogLocalVar)
{
    static uint16_t counter_period = 0;
    uint32_t tempVar;
//    uint32_t localPeriod_debug = 26;
//    tempVar = localPeriod_debug * 512;    // TODO: debug
//    tempVar = tempVar / 1023;
    OUT_SQUARE_RA4_Toggle();    /* toggle square pin to understand if firmware works */

    
    /* localPeriod:1023 = X:convertedValue_filtered */
    tempVar = localPeriod * analogLocalVar->convertedValue;
    tempVar = tempVar / 1023;

    
    /* perform edges */
    counter_period ++;
    if(counter_period > tempVar)    /* reached duty-cycle value */
    {
        OUT_SING_RA5_SetLow();        
    }
    else
    {
        OUT_SING_RA5_SetHigh(); 
    }

    
    /* Reset counter */
    if(counter_period > localPeriod) /* reached period value */
    {
        counter_period = 0;
        OUT_SING_RA5_SetHigh();
    }
    
}
/**
 End of File
*/