/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC12F1822
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set TRIMMER_AN0 aliases
#define TRIMMER_AN0_TRIS                 TRISAbits.TRISA0
#define TRIMMER_AN0_LAT                  LATAbits.LATA0
#define TRIMMER_AN0_PORT                 PORTAbits.RA0
#define TRIMMER_AN0_WPU                  WPUAbits.WPUA0
#define TRIMMER_AN0_ANS                  ANSELAbits.ANSA0
#define TRIMMER_AN0_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define TRIMMER_AN0_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define TRIMMER_AN0_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define TRIMMER_AN0_GetValue()           PORTAbits.RA0
#define TRIMMER_AN0_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define TRIMMER_AN0_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define TRIMMER_AN0_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define TRIMMER_AN0_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define TRIMMER_AN0_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define TRIMMER_AN0_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set JUMPER_RA1 aliases
#define JUMPER_RA1_TRIS                 TRISAbits.TRISA1
#define JUMPER_RA1_LAT                  LATAbits.LATA1
#define JUMPER_RA1_PORT                 PORTAbits.RA1
#define JUMPER_RA1_WPU                  WPUAbits.WPUA1
#define JUMPER_RA1_ANS                  ANSELAbits.ANSA1
#define JUMPER_RA1_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define JUMPER_RA1_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define JUMPER_RA1_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define JUMPER_RA1_GetValue()           PORTAbits.RA1
#define JUMPER_RA1_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define JUMPER_RA1_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define JUMPER_RA1_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define JUMPER_RA1_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define JUMPER_RA1_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define JUMPER_RA1_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set OUT_SQUARE_RA4 aliases
#define OUT_SQUARE_RA4_TRIS                 TRISAbits.TRISA4
#define OUT_SQUARE_RA4_LAT                  LATAbits.LATA4
#define OUT_SQUARE_RA4_PORT                 PORTAbits.RA4
#define OUT_SQUARE_RA4_WPU                  WPUAbits.WPUA4
#define OUT_SQUARE_RA4_ANS                  ANSELAbits.ANSA4
#define OUT_SQUARE_RA4_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define OUT_SQUARE_RA4_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define OUT_SQUARE_RA4_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define OUT_SQUARE_RA4_GetValue()           PORTAbits.RA4
#define OUT_SQUARE_RA4_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define OUT_SQUARE_RA4_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define OUT_SQUARE_RA4_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define OUT_SQUARE_RA4_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define OUT_SQUARE_RA4_SetAnalogMode()      do { ANSELAbits.ANSA4 = 1; } while(0)
#define OUT_SQUARE_RA4_SetDigitalMode()     do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set OUT_SING_RA5 aliases
#define OUT_SING_RA5_TRIS                 TRISAbits.TRISA5
#define OUT_SING_RA5_LAT                  LATAbits.LATA5
#define OUT_SING_RA5_PORT                 PORTAbits.RA5
#define OUT_SING_RA5_WPU                  WPUAbits.WPUA5
#define OUT_SING_RA5_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define OUT_SING_RA5_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define OUT_SING_RA5_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define OUT_SING_RA5_GetValue()           PORTAbits.RA5
#define OUT_SING_RA5_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define OUT_SING_RA5_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define OUT_SING_RA5_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define OUT_SING_RA5_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/