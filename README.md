# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* It generates a square wave with pulse-width modulation or frequency modulation in base of input selected
* Can be used to simulate a transducer that generates a PWM or a FM signal (for example a flowmeter)
*       Target: PIC12F1822
*       Toolchain: 
*                  MPlabX_v5.30; 
*                  XC8_v2.05; 
*                  Packs:12-16F1xxxx_DFP (1.0.42);
*                  Mplab Code Configurator: v3.95.0
*                                           Core v4.75
*                                           PeripheralLibrary v1.77.0
* External contributions or suggestions are welcome
*

### How do I get set up? ###

* Written in C language
* IDE MPLAB X used
* Microcontroller PIC12F1822
* Use a PICKit 3 or 4 In-Circuit Debugger to program the micro
* See SCM_square_wave_generator.svg file to understand how to wire the microcontroller
* Few componets are necessary 
* A trimmer is necessary to vary the frequency or the duty-cycle of the signal
* Set RA1 to VCC to generate a frequency modulation signal on otput RA5
* Set RA1 to GND to generate a pulse width modulation signal on output RA5
* On RA5 toggled every call of pulse_width_modulation_generator() or frequency_modulation_generator() function
* Suggest to use a oscilloscope to test the signals
* 

### Contribution guidelines ###



### Who do I talk to? ###

