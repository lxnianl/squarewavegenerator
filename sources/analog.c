/* 
 * File:   analog.c
 * Author: 
 *
 * Created on 28 dicembre 2019, 22.19
 */

/******************************************************************************
* Includes
******************************************************************************/
#include "variables.h"
#include "init.h"
#include "../mcc_generated_files/mcc.h"
#include "MCCinterface.h"
#include "analog.h"

/******************************************************************************
* Global variables
******************************************************************************/
analogVar_t analogVar;

/******************************************************************************
* Local variables
******************************************************************************/

/******************************************************************************
* Local function prototypes
******************************************************************************/

/******************************************************************************
* Implementation
******************************************************************************/
/**
  @Summary
    ReadAnlog

  @Description
    This routine is used to read the analog channel 
    
  @Preconditions
    ADC_Initialize() function should have been called before calling this function.

  @Returns
    None

  @Param
    analogLocalVar: pointer to analog structure

  @Example
    <code>
    </code>
*/
void ReadAnalog(analogVar_t *analogLocalVar)
{
    if (ADC_IsConversionDone())
    {
        analogLocalVar->convertedValue += ADC_GetConversionResult();
        analogLocalVar->convertedValue = analogLocalVar->convertedValue >> 1;    /* perform incremental filter */
        ADC_SelectChannel(0);   /* select channel */
        ADC_StartConversion();  /* start new conversion */
        analogLocalVar->conversion_timeout = 0;  /* reset timeout */
    }
    else
    {
        /* conversion not completed: wait until max timeout*/
        if (analogLocalVar->conversion_timeout < 10)
        {
            analogLocalVar->conversion_timeout++;
        }
        else
        {
            /* timeout is elapsed: stop current conversion and prepare to restart */
            ADC_Initialize();
            ADC_SelectChannel(0);   /* select channel */
            ADC_StartConversion();  /* start new conversion */
            analogLocalVar->conversion_timeout = 0;
        }
    }

}